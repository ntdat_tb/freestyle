$('#slide-banner').owlCarousel({
    loop:true,
    margin:0,
    nav:true,
    items:1,
    animateOut: 'fadeOut',
    autoplay:true,
    autoplayTimeout:5000,
})
if ($('#imageGallery')) {
    var slider_detail = $('#imageGallery').lightSlider({
        gallery: true,
        autoWidth: false,
        adaptiveHeight:false,
        item: 1,
        loop:true,
        slideMargin: 0,
        thumbItem:5,
        auto:true,
        speed:1000,
        responsive : [
            {
                breakpoint:480,
                settings: {
                    thumbItem:3
                }
            }
        ]
    }); 
}
$('.slideControls .slidePrev').click(function() {
    slider_detail.goToPrevSlide();
});

$('.slideControls .slideNext').click(function() {
    slider_detail.goToNextSlide();
});
// slider FUTURE PRICE
var input_min = $("#input_min");
var input_max = $("#input_max");
var slider = $('#slider');
var min_range = parseInt(slider.attr('attr-min'));
var max_range = parseInt(slider.attr('attr-max'));
function slider_input(input_min,input_max) {
    $('#slider').slider({
        range: true,
        min: min_range,
        max: max_range,
        values: [ input_min, input_max ],
        slide: function(event, ui) {
            if ( ui.values[0] == ui.values[1] ) {
                $('.price-range-both i').css('display', 'none');
            } else {
                $('.price-range-both i').css('display', 'inline');
            }
      }
    });
}
input_min.keyup(function(){
    var number_min = $(this).val();
    var number_max = input_max.val();
    slider_input(number_min,number_max);
});
input_max.keyup(function(){
    var number_max = $(this).val();
    var number_min = input_min.val();
    slider_input(number_min,number_max);
});
// $("#iput").val()
// slider.attr('attr-max')
slider.slider({
    range: true,
    min: min_range,
    max: max_range,
    values: [ input_min.val(), input_max.val() ],
    slide: function(event, ui) {
        input_min.val(ui.values[ 0 ])
        input_max.val(ui.values[ 1 ])
        if ( ui.values[0] == ui.values[1] ) {
            $('.price-range-both i').css('display', 'none');
        } else {
            $('.price-range-both i').css('display', 'inline');
        }
    }
});
$('.ui-slider-range').append('<span class="price-range-both value"></span>');
// $' + $('#slider').slider('values', 0 ) + ' - </i>' + $('#slider').slider('values', 1 ) + '
$('.ui-slider-handle:eq(0)').append('<span class="price-range-min value"></span>');
// $' + $('#slider').slider('values', 0 ) + '
$('.ui-slider-handle:eq(1)').append('<span class="price-range-max value"></span>');
// $' + $('#slider').slider('values', 1 ) + '
input_min.val(slider.slider('values', 0 ))
input_max.val(slider.slider('values', 1 ))


$(document).ready(function() {
});